<?php

function iecho(&$param, $optionalString= '', $condition = true){
    if ($condition)
        if (isset($param))
            if (strlen($optionalString)>0)
                echo $optionalString;
            else echo $param;

}

function ireturn(&$param, $optionalString= '', $condition = true){
    if ($condition)
        if (isset($param))
            if (strlen($optionalString)>0)
                return $optionalString;
            else return $param;
     
    return false;
}

/**
 * If empty first parameter, returns default value (second parameter)
 * 
 * @param type $var
 * @param type $default
 * @param type $filter
 * @return type
 */
function ifEmpty(&$var, $default = null, $filter = null){
    
    return (empty($var)) ? $default : ( (!empty($filter)) ? $filter($var) : $var  ); 
}

/**
 * If not empty first parameter, returns second parameter
 * 
 * @param type $var
 * @param type $default
 * @param type $filter
 * @return type
 */
function ifNotEmpty(&$var, $default = null, $filter = null){
    
    return (!empty($var)) ? $default : ( (!empty($filter)) ? $filter($var) : $var  ); 
}
?>
