<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 * @
 */

// namespace db\redis;

import('db.redis.RedisConnection');

/**
 * Util for cache into redis server.
 *
 * @package db.redis
 * @author ZedPlan Team (opencorephp@zedplan.com)
 * 
 * Use  https://github.com/nrk/phpiredis
 * Inpired http://php5-redis.googlecode.com/svn/trunk/Php5Redis.php
 * 
 */
class RedisCache
{
        static $cacheDB='Redis-cache';
        
        
	public static function getCache($key, $cacheTTL, $compress= false){
            
             if (empty($cacheTTL) || empty($key)){
                return false;
            }
            else{
                $keyName= 'cache-'.sha1($key);

                $cache= DB::getConnection(self::$cacheDB);
                
                if ( $cache->exists($keyName) && ( $cache->ttl($keyName) == $cacheTTL ) ){
                        return ($compress) ? gzinflate($cache->get($keyName)) : $cache->get($keyName) ;
                }
                else{
                    return false;
                }
            }
        }
        
        public static function setCache($key, $data, $cacheTTL , $compress= false, $compression_level= 4){
            if (empty($cacheTTL) || empty($key)){
                return false;
            }
            else{
                $keyName= 'cache-'.sha1($key);
                
                $cache= DB::getConnection(self::$cacheDB);
                
                $cache->set($keyName, ($compress) ? gzdeflate($data, $compression_level) : $data );
                $cache->expire($keyName, $cacheTTL);
                return true;
            }
        }
}
?>
