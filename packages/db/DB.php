<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */


// namespace db;

import("db.QueryBuilder");

/**
 * Class for creating database connections and performing common db tasks.
 *
 * @package db
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
final class DB
{
	/**
	 * @var Connection[]
	 */
	private static $instances = array();
	
	private function __construct() { }
	
	/**
	 * Get connection for specified database name.
	 * Database connection details must be configured in the "db.php" config file.
	 *
	 * @param string $ConnName
	 * @return Connection
	 * @throws InvalidArgumentException if specified connection name does not exist.
	 * @throws FileNotFoundException if directory where class is searched is invalid.
	 * @throws ClassNotFoundException if unable to load driver's class.
	 * @throws ClassDefNotFoundException if the class definition could not be found.
	 * @throws SQLException if any connection / database error accurs.
	 */
	public static function getConnection($ConnName = 'default')
	{
		if (!isset(self::$instances[$ConnName])) {
			$config = Config::getInstance();
			if (!$config->exists("db.$ConnName")) {
				throw new InvalidArgumentException("'$ConnName' is not a valid connection name.");
			}
			$data = (array)$config->get("db.$ConnName");
			$className = ucfirst($data['driver']) . "Connection";
			Loader::loadClass("db\\{$data['driver']}\\$className", FRAMEWORK_DIR . '/packages');
			self::$instances[$ConnName] = new $className();
                        if ( $data['driver']!='redis' ){
                            self::$instances[$ConnName]->connect(
                                    $data['host'], $data['username'], $data['password'], $data['dbname'], $data['port']
                            );
                            self::$instances[$ConnName]->setCharset($data['charset']);
                        }else{
                            self::$instances[$ConnName]->connect( $data['host'], $data['port'], $data['password']);
                        }
		}
		return self::$instances[$ConnName];	
	}
	
	/**
	 * Parse DSN string and return array with its parts.
	 *
	 * @param string $dsn
	 * @return string[]
	 */
	public static function parseDSN($dsn)
	{
		$matches = array();
		if (preg_match("#^(\\w+)://(.+?):(.*?)@(.+?)(?:/(.+))?$#i", $dsn, $matches)) {
			return array(
				'driver'	=> $matches[1],
				'host'		=> $matches[2],
				'username'	=> $matches[3],
				'password'	=> $matches[4],
				'dbname'	=> isset($matches[5]) ? $matches[5] : ''
			);
		}
		return null;
	}
	
	/**
	 * This method is deprecated. Use QueryBuilder#createInsert instead
	 *
	 * @param string $table Db table
	 * @param array $fields Array where keys are column names.
	 * @param string $fieldPrefix prefix for column names.
	 * @return string
	 * @deprecated
	 * @see QueryBuilder#createInsert
	 */
	public static function createInsert($table, array $fields, $fieldPrefix = '')
	{
		return QueryBuilder::create()->createInsert($table, $fields, $fieldPrefix);
	}
	
	/**
	 * This method is deprecated. Use QueryBuilder#createReplace instead
	 *
	 * @param string $table Db table
	 * @param array $fields Array where keys are column names.
	 * @param string $fieldPrefix prefix for column names.
	 * @return string
	 * @deprecated
	 * @see QueryBuilder#createReplace
	 */
	public static function createReplace($table, array $fields, $fieldPrefix = '')
	{
		return QueryBuilder::create()->createReplace($table, $fields, $fieldPrefix);
	}
	
	/**
	 * This method is deprecated. Use QueryBuilder#createUpdate instead
	 *
	 * @param string $table Db table
	 * @param array $fields Array where keys are column names.
	 * @param string $where SQL WHERE as string excluding the WHERE keyword.
	 * @param string $fieldPrefix prefix for column names.
	 * @return string
	 * @deprecated
	 * @see QueryBuilder#createUpdate
	 */
	public static function createUpdate($table, array $fields, $where = '', $fieldPrefix = '')
	{
		return QueryBuilder::create()->createUpdate($table, $fields, $where, $fieldPrefix);
	}
}
?>
