<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2012, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */


// namespace db;

import("db.DB");

class BaseSQL {
    
    static protected $returnArray = true; // choose between array or object
    static protected $dbName = 'default'; // overwrite database connection
    
    static protected function doIQuery($sql, $getOne= false, $dbName='', $cacheTTL=null){
        
        try{            
    
            if (empty($dbName)) $dbName=self::$dbName;
            $db = DB::getConnection($dbName);

            if (empty($sql)) return FALSE;

            if (empty($cacheTTL)){
                $rst = $db->query($sql);
            }
            elseif (! $rst= RedisCache::getCache($sql, $cacheTTL) ){
                $rst = $db->query($sql);

                RedisCache::setCache($sql, $rst, $cacheTTL);
            }

          //  fb($sql);

            if ($rst->rowCount() > 0)
                if ($getOne){
                    $data= (self::$returnArray) ? $rst->fetch() : $rst->fetchObject();
                    $rst->free();
                    return $data;
                }
                else {
                    $data= (self::$returnArray) ? $rst->fetchAll() : $rst->fetchAllObjects();
                    $rst->free();
                    return $data;
                }
            else{
                $rst->free();
                return FALSE;   

                }
        } catch( Exception $e ) {
            throw $e;
        }
    }
    
    static protected function doICall($procedureName, $type, $params='', $getOne= false, $dbName=''){
        if (empty($dbName)) $dbName=self::$dbName;
        $db = DB::getConnection($dbName);
        
        if (empty($procedureName) || empty($type) ) return FALSE;
        
        if ($type=='exec'){
            $rst = $db->callExec($procedureName, $params);
        }elseif ($type=='query'){
            $rst = $db->callQuery($procedureName, $params);
        }else return FALSE;

        
        if ($rst->rowCount() > 0)
            if ($getOne){
                $data= (self::$returnArray) ? $rst->fetch() : $rst->fetchObject();
                $rst->free();
                return $data;
            }
            else {
                $data= (self::$returnArray) ? $rst->fetchAll() : $rst->fetchAllObjects();
                $rst->free();
                return $data;
            }
        else{
            $rst->free();
            return FALSE;   
            
            }
    }
    
    static protected function doIExec($sql, $dbName=''){
        if (empty($dbName)) $dbName=self::$dbName;
        $db = DB::getConnection($dbName);
        
        if (empty($sql)) return FALSE;
        
        $rst = $db->exec($sql);
           
        $lastInsertId = $db->lastInsertId();
        if( $lastInsertId != 0)
            return $lastInsertId;
        elseif ($rst >= 0)
            return $rst;
        else
            return FALSE;
    }
    
    static protected function doIMultiExec($sql, $dbName=''){
        if (empty($dbName)) $dbName=self::$dbName;
        $db = DB::getConnection($dbName);
        
        if (empty($sql)) return FALSE;
        
        $rst = $db->multiExec($sql);
          
        $lastInsertId = $db->lastInsertId();
        if( $lastInsertId != 0)
            return $lastInsertId;
        elseif (is_array($rst) && count($rst) > 0)
            return $rst;
        else
            return FALSE;
    }         
            
    
    
    static public function php2sql($value){
        
        if(is_null($value)) 
            return var_export($value, true);
        else
            return $value;
        
    }
    
    static public function doIQuote($input, $characters = '', $dbName=''){
        if (empty($dbName)) $dbName=self::$dbName;
        $db = DB::getConnection($dbName);
        
        return $db->quote($input, $characters);
    }
            
}

?>
