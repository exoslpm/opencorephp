<?
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */


//namespace io;

import("io.IOException");

/**
 * Signals that an attempt to open the file denoted by a specified pathname has failed.
 * This exception will be thrown when a file with the specified pathname does not exist or for some reason is inaccessible. 
 * 
 * @exception 
 * @package io
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
class FileNotFoundException extends IOException
{
	/**
	 * Constructor
	 *
	 * @param string $msg
	 */
	function __construct($msg = '')
	{
		parent::__construct($msg);
	}
}
?>