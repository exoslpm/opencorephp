<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */
?>

<h1><?=_("Login")?></h1>
<?
	echo HTML::uList($this->errors, null, 'errors');
?>
<form method="post" action="" onsubmit="return $(this).validate()">
	<dl>
		<dt><?=_("Username")?>:</dt>
		<dd><input type="text" name="username" id="username" value="" />
		<!-- type=string required=true errormsg="<?=_("Invalid username")?>" --></dd>
		<dt><?=_("Password")?>:</dt>
		<dd><input type="password" name="password" id="password" value="" /></dd>
	</dl>
	<div><input type="submit" value="<?=_("Login")?>" /></div>
</form>
